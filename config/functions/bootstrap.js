'use strict';

const mysql      = require('mysql');

/**
 * An asynchronous bootstrap function that runs before
 * your application gets started.
 *
 * This gives you an opportunity to set up your data model,
 * run jobs, or perform some special logic.
 *
 * See more details here: https://strapi.io/documentation/v3.x/concepts/configurations.html#bootstrap
 */
const TelegramBot = require('node-telegram-bot-api'); 
module.exports = async () => {
    const myDBPool = mysql.createPool({
        host: strapi.config.database.connections.default.settings.host,
        user: strapi.config.database.connections.default.settings.username,
        password : strapi.config.database.connections.default.settings.password,
        connectionLimit: 5,
        database: strapi.config.database.connections.default.settings.database,
        debug: true
    });
    console.log("strapi.config : ",strapi.config.database.connections.default.settings);
    
    // const token = '1146126075:AAGdfjLfe51h4E-npAqhCWbjYZRCC0shHRo';
    // const token = '1441442870:AAE02ZgvANY9BSH1lqttaQqLNuSd9lrASPo'; //ctb_local
    const token = "1200417071:AAFK61NTPDaOE29e-3UzJylPgmud-4S7kiw"; //handal_bot
    const bot = new TelegramBot(token, {polling: true});
    // const bot = new TelegramBot(token);
    bot.on("polling_error", console.log);
    console.log("hai jude !");
    // Listener (handler) for telegram's /label event
    var chatId = "";
    var url = "";
    var checkAuthenticated = async function(msg, match){
        chatId = msg.chat.id;
        var detailOperator = await strapi.query('ctb-operator').findOne({ telegram_id: chatId});
        if(detailOperator == null){
            bot.sendMessage(
                chatId,
                'Silahkan Registrasikan diri anda',
                {
                    reply_markup: {
                        inline_keyboard: [[
                            {
                                text: 'Register',
                                callback_data: 'register'
                            }, 
                            {
                                text: 'Cancel',
                                callback_data: 'no_register'
                            },  
                        ]]
                    }
                }
            );
        }else if(!detailOperator.active){
            bot.sendMessage(chatId,`Hallo ${detailOperator.nama_petugas}, Akun anda sedang dalam proses verifikasi admin, mohon ditunggu.`);
            return null;
        }
        return detailOperator;
    }

    const getListWO = async function(message, match){
        var operator = await checkAuthenticated(message);
        if(operator == null){
            // respWoMap = "anda belumterautnetikasi";
            // return;
        }else{
            let qlistwo = `select *,customers.id as id_customer, working_orders.id as id_wo from customers left join (select customers.id, count(*) jml_incident from customers join incidents on incidents.customer = customers.id group by customers.id) t on customers.id = t.id join working_orders on working_orders.customer = customers.id and customers.ctb_area = '${operator.ctb_area.id}'`;
            console.log('qlistwo : ', qlistwo);
            await myDBPool.query(qlistwo, async function (error, listWo, fields) {
                console.log("listWo :", listWo);
                if (error){
                    console.log("error getListWO : ", error);
                }else{
                    // var listWo = await strapi.query('working-order').find();
                    // console.log("listWo", listWo);
                    var respWo = '';
                    if(listWo.length > 0){
                        respWo = `Berikut adalah list WO, silahkan click \`/detail_wo_x\` untuk melihat detail : \n`;
                        for(let i=0; i < listWo.length; i++){
                            var billingUser = await strapi.query('billings').find({ customer: listWo[i].id_customer });
                            console.log("billingUser : ",billingUser)
                            if(billingUser.length > 0){
                                let totalBilling = 0;
                                for(let j=0; j < billingUser.length; j++){
                                    // console.log(listWo[i].customer_name,billingUser[j].amount);
                                    // console.log("billingUser : ", billingUser[j]);
                                    totalBilling = totalBilling + billingUser[j].amount;
                                }
                                let prioritas = '-';
                                if((listWo[i].jml_incident == 0 || listWo[i].jml_incident == null) && listWo[i].usage_gb > 15 && totalBilling > 200000){
                                    prioritas = 'P1';
                                }
                                else if((listWo[i].jml_incident < 4 || listWo[i].jml_incident == null) && listWo[i].usage_gb >= 15 && totalBilling > 200000){
                                    prioritas = 'P2';
                                }
                                else if((listWo[i].jml_incident < 4 || listWo[i].jml_incident == null) && listWo[i].usage_gb >= 11 && listWo[i].usage_gb <= 15 && totalBilling > 500000){
                                    prioritas = 'P3';
                                }
                                else if((listWo[i].jml_incident < 4 || listWo[i].jml_incident == null) && listWo[i].usage_gb >= 6 && listWo[i].usage_gb <= 10){
                                    prioritas = 'P4';
                                }
                                else if(listWo[i].usage_gb < 5){
                                    prioritas = 'P5';
                                }
                                console.log({
                                    "id_customer": listWo[i].id_customer,
                                    "customer_name": listWo[i].customer_name,
                                    "id_wo": listWo[i].id_wo,
                                    "billings" : totalBilling,
                                    "incident" :listWo[i].jml_incident,
                                    "usage" : listWo[i].usage_gb,
                                    "prioritas" : prioritas
                                });
                                respWo += `\n/detail_wo_${listWo[i].id_wo} [${prioritas}] - ${listWo[i].snd} - ${listWo[i].customer_name}\n`
                            }
                        }
                    }else{
                     respWo = 'Saat ini tidak ada WO yang aktif di area anda';
                    }
                    
                    console.log("respWo", respWo)
                    bot.sendMessage(message.chat.id, respWo, {
                        reply_markup: {
                            inline_keyboard: [[
                                {
                                    text: 'Back',
                                    callback_data: 'home'
                                }, 
                            ]]
                        }
                    });
                }
            });
        }
    }
    var welcome = async function (msg, match) {
        var operator = await checkAuthenticated(msg, match);
        console.log("====>> Operator", operator);
        if(operator == null){
           return;
        }else{
            chatId = msg.chat.id;
            bot.sendMessage(
                chatId,
                `Hallo ${operator.nama_petugas}, Selamat Datang di CTB Bot Agent. \nSIlahkan klik menu di bawah : `,
                {
                    reply_markup: {
                        inline_keyboard: [[
                            {
                                text: 'WO',
                                callback_data: 'wo'
                            }, 
                            {
                                text: 'Mapping',
                                callback_data: 'map'
                            },  
                        ]]
                    }
                }
            );
            return;
        }
    }

    const botDetailIncident = async function(msg, match){
        var operator = await checkAuthenticated(msg, match);
        console.log("====>> Operator", operator);
        if(operator == null){
           return;
        }else{
            chatId = msg.chat.id;
            var id_ticket = match.input.split('_')[2];
            
            bot.sendMessage(
                chatId,
                'id_ticket is '+id_ticket,
            );
            return;
        }
    }

    const botDetailPackage = async function(msg, match){
        chatId = msg.chat.id;
        var id_ticket = match.input.split('_')[2];
        bot.sendMessage(
            chatId,
            'id_package is '+id_ticket,
        );
        return;
    }

    const askRegister = async function(msg, match){
        let requestPhoneKeyboard = {
            "reply_markup": {
                "one_time_keyboard": true,
                "keyboard": [[{
                    text: "My phone number",
                    request_contact: true,
                    one_time_keyboard: true,
                    callback_data : 'phone'
                }], ["Cancel"]]
            }
         };
        console.log("======> Ask Register msg.chat :", msg.chat);
        bot.sendMessage(msg.chat.id, 'Can we get access to your phone number?', requestPhoneKeyboard);
        return;
    }

    const askLocation = async function(msg, match){
        let requestPhoneKeyboard = {
            "reply_markup": {
                "one_time_keyboard": true,
                "keyboard": [[{
                    text: "Location",
                    request_location: true,
                    one_time_keyboard: true,
                    callback_data : 'location'
                }], ["Cancel"]]
            }
         };
        console.log("======> Ask Register msg.chat :", msg.chat);
        bot.sendMessage(msg.chat.id, 'Can we get access to your location?', requestPhoneKeyboard);
        return;
    }

    const botDetailWO = async function (msg, match){
        var operator = await checkAuthenticated(msg, match);
        console.log("====>> Operator", operator);
        if(operator == null){
           return;
        }else{
            chatId = msg.chat.id;
            var id_wo = match.input.split('_')[2];
            var detailWO = await strapi.query('working-order').findOne({ id: id_wo });
            var paketData = ``;
            var gangguan = ``;
            var jmlTagihan = 0;
            
            for (let index = 0; index < detailWO.billings.length; index++) {
                jmlTagihan = jmlTagihan + detailWO.billings[index].amount;
                
            }

            var customerWO = await strapi.query('customers').findOne({ id: detailWO.customer.id });
            console.log("customerWO : ", customerWO);
            for (let index = 0; index < customerWO.packages.length; index++) {
                paketData = `${paketData} ${customerWO.packages[index].package_name},`;
                
            }

            for (let index = 0; index < customerWO.incidents.length; index++) {
                gangguan = `    
            #${index+1}. ${customerWO.incidents[index].incident_name}
            Status : ${customerWO.incidents[index].incident_name}
            Worklog : ${customerWO.incidents[index].last_updated_worklog}`;
                
            }


            var respDetailWO = `
No. Inet : ${detailWO.snd}
Nama : ${detailWO.customer.customer_name}
Alamat : ${detailWO.customer.alamat}
Paket Data : ${paketData}
Gangguan :${gangguan}
Jml Tagihan : ${jmlTagihan}
Penggunaan Data : ${detailWO.customer.usage_gb}GB
Lokasi : https://www.google.com/maps/search/?api=1&query=${detailWO.customer.lon},${detailWO.customer.lat}
            `;
            bot.sendMessage(
                chatId,
                respDetailWO,
                {
                    reply_markup: {
                        inline_keyboard: [[
                            {
                                text: 'Back',
                                callback_data: 'wo'
                            },
                            {
                                text: 'Home',
                                callback_data: 'home'
                            },  
                        ]]
                    }
                }
            );
            bot.sendLocation(chatId,detailWO.customer.lon, detailWO.customer.lat);
            return;
        }
    }

    const botCallback = async function (callbackQuery) {
        console.log("callbackQuery", callbackQuery)
        var message = callbackQuery.message;
        var category = callbackQuery.data;
        
        switch (category) {
            case 'list_incident':
                var listIncident = await strapi.query('incident').find();
                // console.log("listIncident", listIncident);
                let respIncident = `Berikut adalah list incident, silahkan click \`/detail_ticket_x\` untuk melihat detail : \n`;
                for(let i=0; i < listIncident.length; i++){
                    respIncident += `\n/detail_ticket_${listIncident[i].id} [${listIncident[i].customer.customer_name}] - ${listIncident[i].incident_name}\n`
                }
                console.log("respIncident", respIncident)
                bot.sendMessage(message.chat.id, respIncident);
                break;
            case 'wo':
                getListWO(message);
                break;
            case 'map':
                askLocation(message);
                // var listWo = await strapi.query('working-order').find();
                // // console.log("listWo", listWo);
                // let respWoMap = `Saat ini tidak ada WO tersedia \n`;
                // var markers = '';
                // for(let i=0; i < listWo.length; i++){
                //     markers += `/${listWo[i].customer.lon},${listWo[i].customer.lat}`
                // }
                
                // if(markers != ''){
                //     respWoMap = `https://www.google.com/maps/dir${markers};`
                // }
                
                // console.log("respWoMap", respWoMap)
                // bot.sendMessage(message.chat.id, respWoMap, {
                //     reply_markup: {
                //         inline_keyboard: [[
                //             {
                //                 text: 'Back',
                //                 callback_data: 'home'
                //             }, 
                //         ]]
                //     }
                // });
                break;
            case 'packeges':
                var listPackages = await strapi.query('package').find();
                // console.log("listPackages", listPackages);
                let respPackages = `Berikut adalah list package, silahkan click \`/detail_package_x\` untuk melihat detail : \n`;
                for(let i=0; i < listPackages.length; i++){
                    respPackages += `\n/detail_package_${listPackages[i].id}  - ${listPackages[i].package_name}`
                }
                console.log("respPackages", respPackages)
                bot.sendMessage(message.chat.id, respPackages);
                break;
            case 'customer':
                var listCustomers = await strapi.query('customers').find();
                // console.log("listCustomers", listCustomers);
                let respCustomers = 'Berikut adalah list customer, silahkan click /detail_customer_x untuk melihat detail :* \n';
                for(let i=0; i < listCustomers.length; i++){
                    respCustomers += `\n/detail_customer_${listCustomers[i].id}  - ${listCustomers[i].customer_name}`
                }
                console.log("respCustomers", respCustomers);
                bot.sendMessage(message.chat.id, respCustomers,{ parseMode: 'HTML' });
                break;
            case 'home':
                console.log("case HOME")
                welcome(message, null);
                break;
            case 'register':
                console.log("case REGISTER")
                askRegister(message, null);
                break;
            case 'no_register':
                console.log("case No REGISTER")
                welcome(message, null);
                break;
            case 'phone':
                console.log("======>> REGISTER Phone");
                console.log("message : ", message);
                // welcome(message, null);
                break;
            default:
                bot.sendMessage(message.chat.id, `menu not found`);
                break;
        }
    }
    // Handler for phone number request when user gives permission
    bot.on('contact', async (msg) => {
        var phone = msg.contact.phone_number;
        console.log("====>>> msg.chat : ", msg.chat);
        bot.sendMessage(msg.chat.id, `Phone number saved: ${phone}`);
        var ctbOperator = {
            "nama_petugas" : `${msg.chat.first_name} ${msg.chat.last_name}`,
            "telegram_id" :  msg.chat.id,
            "no_hp" : phone
        };
        // var validData = await strapi.entityValidator.validateEntity(strapi.models['ctb-operator'], ctbOperator);
        console.log("==========>>> ctbOperator :", ctbOperator);
        var entry = await strapi.query('ctb-operator').create(ctbOperator);
        console.log("==========>>> ctbOperator entry", entry)
    });


    bot.on('location', async (msg) => {
        
        var operator = await checkAuthenticated(msg);
        if(operator == null){
            // respWoMap = "anda belumterautnetikasi";
            // return;
        }else{
            console.log("operator : ", operator);
            var message = msg;
            console.log("test location", msg);
            console.log(msg.location.latitude);
            console.log(msg.location.longitude);
            let respWoMap = `Saat ini tidak ada WO tersedia \n`;    
            var markers = '';
            let qlistwo = `SELECT customers.id,customers.customer_name,customers.lon, customers.lat, alamat, area_name, ( 3959 * acos ( cos ( radians(${msg.location.longitude}) ) * cos( radians( lat ) ) * cos( radians( lon ) - radians(${msg.location.latitude}) )+ sin ( radians(${msg.location.longitude}) )* sin( radians( lat ) ))) AS distance FROM customers join ctb_areas on customers.ctb_area = ctb_areas.id and customers.ctb_area = '${operator.ctb_area.id}' order by distance asc`;
            console.log("qlistwo : ", qlistwo);
            await myDBPool.query(qlistwo, async function (error, listWo, fields) {
                if (error){
                    console.log("error getListWO Mapping : ", error);
                }else{
                    // console.log("listWo", listWo);
                    if(listWo.length > 0){
                        markers += `/${msg.location.latitude},${msg.location.longitude}`
                    }
                    for(let i=0; i < listWo.length; i++){
                        if(listWo[i].distance){
                            markers += `/${listWo[i].lon},${listWo[i].lat}`;
                        }
                    }
                    if(markers != ''){
                        respWoMap = `https://www.google.com/maps/dir${markers};`;
                    }
                }
                bot.sendMessage(message.chat.id, respWoMap, {
                    reply_markup: {
                        inline_keyboard: [[
                            {
                                text: 'Back',
                                callback_data: 'home'
                            }, 
                        ]]
                    }
                });
            });
        }
    });

    bot.onText(/\/help/, welcome);

    bot.onText(/\/start/, welcome);

    bot.onText(/\/detail_ticket_*/, botDetailIncident);

    bot.onText(/\/detail_package_*/, botDetailPackage);

    bot.onText(/\/detail_wo_*/, botDetailWO);

    // Listener (handler) for callback data from /label command
    bot.on('callback_query', botCallback);
};


