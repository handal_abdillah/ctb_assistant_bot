module.exports = ({ env }) => ({
  defaultConnection: 'default',
  connections: {
    default: {
      connector: 'bookshelf',
      settings: {
        client: 'mysql',
        host: env('DATABASE_HOST', process.env.DB_HOST || '127.0.0.1'),
        port: env.int('DATABASE_PORT', process.env.DB_PORT ||3306),
        database: env('DATABASE_NAME', process.env.DB_NAME || 'ctb_assistant_bot'),
        username: env('DATABASE_USERNAME', process.env.DB_USER || 'root'),
        password: env('DATABASE_PASSWORD', process.env.DB_PASS || ''),
        ssl: env.bool('DATABASE_SSL', false),
      },
      options: {}
    },
  },
});
